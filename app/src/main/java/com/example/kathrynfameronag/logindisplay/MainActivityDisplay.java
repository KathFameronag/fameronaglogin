package com.example.kathrynfameronag.logindisplay;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivityDisplay extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_display);

        Intent in = getIntent();
        String tv1= in.getExtras().getString("message");
        String tv2= in.getExtras().getString("message");

        TextView UN = (TextView) findViewById(R.id.textView1);
        UN.setText(tv1);

        TextView PW = (TextView) findViewById(R.id.textView2);
        PW.setText(tv2);
    }
}
